# CF-Interactor #

Launch and list CloudFront distributions. 

Launching: Uses the same configuration for each distribution. This tool is likely not useful outside of simply testing where use cases warrant having a large number of distributions.

Listing: Paginated listing of distributions. Not terribly clean, but it works well when you have a large number of key-value pairs coming back to you in the response.

### Dependencies ###

```
#!scala

libraryDependencies += "com.amazonaws" % "aws-java-sdk" % "1.11.+"
```

### Contact ###

* Joseph Greene - ClydeMachine@