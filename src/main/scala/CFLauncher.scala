import com.amazonaws.services.cloudfront._
import com.amazonaws.AmazonServiceException
import com.amazonaws.services.cloudfront.model._

object CFLauncher {
  def main(args: Array[String]): Unit = {
    // Launch 50 CloudFront distributions.

    /* This code is wildly inefficient as it'll make distributions as fast as the rate-limiting on API calls will allow, but does not
    * have any kind of back-off intelligence - it'll keep hitting it as much as it can. So do be careful.
    * Relies on your environment variables being set correctly for CloudFront API access on your AWS account.
    */

    val distrib: AmazonCloudFrontClient = new AmazonCloudFrontClient()


    for (i <- 1 to 50) {
      try {

        println("========= " + i + " being created...")

        val customoriginconfig: CustomOriginConfig = new CustomOriginConfig()
          .withHTTPPort(80)
          .withHTTPSPort(443)
          .withOriginProtocolPolicy("http-only")

        val originserver: Origin = new Origin()
          .withDomainName("a-practice-bucket.s3-website-us-west-2.amazonaws.com")
          .withId("AutoGen-cf" + i)
          .withCustomOriginConfig(customoriginconfig)

        val origins: Origins = new Origins()
          .withItems(originserver)
          .withQuantity(1)

        val allowedmethods: AllowedMethods = new AllowedMethods()
          .withQuantity(2)
          .withItems("HEAD","GET")

        val headers: Headers = new Headers()
          .withQuantity(0)

        val cookies: CookiePreference = new CookiePreference()
          .withForward("none")

        val forwardvals: ForwardedValues = new ForwardedValues()
          .withQueryString(false)
          .withHeaders(headers)
          .withCookies(cookies)

        val trustedsigners: TrustedSigners = new TrustedSigners()
          .withEnabled(false)
          .withQuantity(0)

        val defaultbehaviour = new DefaultCacheBehavior()
          .withAllowedMethods(allowedmethods)
          .withDefaultTTL(86400: Long)
          .withMinTTL(0: Long)
          .withMaxTTL(86400: Long)
          .withCompress(false)
          .withTargetOriginId("AutoGen-cf" + i)
          .withForwardedValues(forwardvals)
          .withViewerProtocolPolicy("allow-all")
          .withTrustedSigners(trustedsigners)


        val distribconfig: DistributionConfig = new DistributionConfig()
          .withComment("Auto-generated distribution #" + i)
          .withEnabled(false)
          .withOrigins(origins)
          .withCallerReference("Auto-generated distribution config for distribution #" + i)
          .withDefaultCacheBehavior(defaultbehaviour)

        val distribrequest: CreateDistributionRequest = new CreateDistributionRequest(distribconfig)
        distrib.createDistribution(distribrequest)

        println("========= " + i + " being created...")

      } catch {
        case ase: AmazonServiceException => println("Error: " + ase)
      } finally {
        println("Done!")
      }
    }
  }
}
