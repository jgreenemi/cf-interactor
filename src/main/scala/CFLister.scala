import com.amazonaws.services.cloudfront._
import com.amazonaws.AmazonServiceException
import com.amazonaws.services.cloudfront.model._

/*
* List all my distributions.
*/

object CFLister {

  def time[R](block: => R): R = {
    // Time your methods.
    // Usage: time(functionname(params))
    val t0 = System.nanoTime()
    val result = block
    val t1 = System.nanoTime()
    println("Elapsed: " + (t1 - t0)/1000000 + "ms")
    result
  }

  def main(args: Array[String]): Unit = {
    val cfclient: AmazonCloudFrontClient = new AmazonCloudFrontClient()
    val listdistribsreq: ListDistributionsRequest = (new ListDistributionsRequest).withMaxItems("1")
    var result: ListDistributionsResult = null

    var i: Int = 0;

    try {
      do {
        i += 1
        result = cfclient.listDistributions(listdistribsreq)
        listdistribsreq.withMarker(result.getDistributionList.getNextMarker)
        println(i + ". " + result)
      } while (result.getDistributionList.isTruncated == true)
    } catch {
      case ase: AmazonServiceException => println("AmazonServiceException! Error: " + ase)
    }

    println("\nCompleted listing " + i + " distributions.")
  }
}

