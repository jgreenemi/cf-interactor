import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.cloudfront.*;
import com.amazonaws.services.cloudfront.model.*;

import java.util.List;

public class CFListerJava {

    public static AmazonCloudFrontClient cfclient = new AmazonCloudFrontClient();
    public static ListDistributionsRequest listdistribsreq = (new ListDistributionsRequest()).withMaxItems("1");
    public static ListDistributionsResult result = null;
    public static int i = 0;

    public static void main(String args[]) {
        try {
            System.out.println("Listing distributions using default system credentials...");
            do {
                i++;

                //Populate the result object with result of the ListDistributionsRequest, with one item maximum.
                result = cfclient.listDistributions(listdistribsreq);

                //Set the ListDistributionsRequest to use the NextMarker to iterate through the list of distributions.
                listdistribsreq.withMarker(result.getDistributionList().getNextMarker());

                //Create a List object of just the distribution information (easier to read than the full request data).
                List resultList = result.getDistributionList().getItems();

                //Print the current distribution's information.
                System.out.println(i + ". " + resultList.toString());
            } while (result.getDistributionList().isTruncated() == true);
        } catch (AmazonServiceException ase) {
            System.out.println("Caught AmazonServiceException: " + ase);
        } finally {
            System.out.println("\nCompleted listing " + i + " distributions.");
        }
    }
}
